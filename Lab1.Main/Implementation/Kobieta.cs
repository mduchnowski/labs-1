﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Kobieta:IKobieta
    {
        public Kobieta() { }

        public string Czynnosc()
        {
            return "tanczy";
        }

        public string Makijaz()
        {
            return "maluje usta";
        }


        public string Transport()
        {
            return "rower";
        }
    }
}
