﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;
using Lab1.Main;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ICzlowiek);
        
        public static Type ISub1 = typeof(IMezczyzna);
        public static Type Impl1 = typeof(Mezczyzna);
        
        public static Type ISub2 = typeof(IKobieta);
        public static Type Impl2 = typeof(Kobieta);
        
        
        public static string baseMethod = "Czynnosc";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Sport";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Makijaz";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "ListaLudzi";
        public static string collectionConsumerMethod = "CoRobia";

        #endregion

        #region P3

        public static Type IOther = typeof(IKosmita);
        public static Type Impl3 = typeof(Kosmita);

        public static string otherCommonMethod = "Transport";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
