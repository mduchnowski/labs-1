﻿using System;
using System.Collections.Generic;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{

    interface IKosmita
    {
        string Transport();
        string Badania();
    }

    class Kosmita : IKosmita, IMezczyzna
    {
        public Kosmita() { }


        public string Transport()
        {
            return "pojazd kosmiczny";
        }

        string ICzlowiek.Transport()
        {
            return "samochód";
        }

        string IKosmita.Transport()
        {
            return "teleporter";
        }


        public string Czynnosc()
        {
            return "gra";
        }


        public string Badania()
        {
            return "na krowach";
        }

        public string Sport()
        {
            return "koszykówka";
        }
    }

    public class Program
    {
        public static IList<ICzlowiek> ListaLudzi()
        {
            IList<ICzlowiek> lista = new List<ICzlowiek> {};
            lista.Add(new Kobieta());
            lista.Add(new Mezczyzna());
            return lista;
        }

        public static void CoRobia(IList<ICzlowiek> czlowiek)
        {
            foreach (ICzlowiek cz in czlowiek)
            {
                Console.WriteLine(cz.GetType().Name + ": " + cz.Czynnosc());
            }
        }

        static void Main(string[] args)
        {
            IList<ICzlowiek> List = ListaLudzi();
            CoRobia(List);

            Console.ReadKey(); 
        }
    }
}
